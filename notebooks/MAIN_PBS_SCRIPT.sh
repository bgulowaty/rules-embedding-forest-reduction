#!/bin/bash
#PBS -q main
#PBS -l walltime=24:00:00
#PBS -l select=1:ncpus=8:mem=16GB
#PBS -l software=python3
#PBS -m n

module load gcc/7.4.0

echo $PBS_O_WORKDIR
cd $PBS_O_WORKDIR

eval ${CMD}