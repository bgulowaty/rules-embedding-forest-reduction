#!/bin/bash
#PBS -q main
#PBS -l walltime=6:00:00
#PBS -l select=1:ncpus=4:mem=2048MB
#PBS -l software=numpy
#PBS -m n

# wejscie do katalogu, z ktorego zostalo wstawione zadania
cd $PBS_O_WORKDIR

# uruchom program 
# z PRZEKIEROWANIEM ZAPISYWANIA WYNIKOW -- BARDZO WAZNE

module load gcc/7.4.0 >& wynik.txt
module load glibc/2.14.1  >& wynik.txt

source /home/bogul/miniforge3/bin/activate >& wynik.txt
poetry install >& wynik.txt 
